#include <iostream>
#include <vector>
#include <queue>

struct IGraph {
    virtual ~IGraph() = default;

    virtual void AddEdge(int from, int to) = 0;
    virtual int VerticesCount() const = 0;

    virtual std::vector<int> GetNextVertices(int vertex) const = 0;
    virtual std::vector<int> GetPrevVertices(int vertex) const = 0;
};

class ListGraph : public IGraph {
public:
    explicit ListGraph(int verticesCount);
    explicit ListGraph(const IGraph &source);

    ~ListGraph() override = default;

    void AddEdge(int from, int to) override;
    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;
    std::vector<int> GetPrevVertices(int vertex) const override;

private:
    std::vector<std::vector<int>> NodesTable;
};

ListGraph::ListGraph(int verticesCount) : NodesTable(verticesCount) {}

ListGraph::ListGraph(const IGraph &source) : NodesTable(source.VerticesCount()) {
    for (int i = 0; i < source.VerticesCount(); i++) {
        NodesTable[i] = source.GetNextVertices(i);
    }
}

void ListGraph::AddEdge(int from, int to) {
    if (from < 0 || to < 0) {
        return;
    }

    int verticesCount = NodesTable.size();

    if (from >= verticesCount || to >= verticesCount ) {
        return;
    }

    NodesTable[from].push_back(to);
}

int ListGraph::VerticesCount() const {
    return NodesTable.size();
}

std::vector<int> ListGraph::GetNextVertices(int vertex) const {
    return NodesTable[vertex];
}

std::vector<int> ListGraph::GetPrevVertices(int vertex) const {
    std::vector<int> result = {};

    if (vertex < 0 || vertex >= NodesTable.size()) {
        return result;
    }

    int verticesCount = NodesTable.size();

    for (size_t from = 0; from < verticesCount; from++) {
        int currentVertexPairs = NodesTable[from].size();

        for (size_t i = 0; i < currentVertexPairs; i++) {
            if (NodesTable[from][i] == vertex) {
                result.push_back(from);
            }
        }
    }
}

int taskResult(const IGraph &graph, int from, int to) {
    // first in pair - depth
    // second in pair - paths count to vertex
    std::vector<std::pair<int, int>> vertexInfo(graph.VerticesCount(), {0, 0});

    std::queue<int> queue;
    queue.push(from);

    while (!queue.empty()) {
        int current = queue.front();
        queue.pop();

        vertexInfo[from].second = 1;

        std::vector<int> nextVertices = graph.GetNextVertices(current);

        for (int v : nextVertices) {
            if (vertexInfo[v].second == 0) {
                queue.push(v);
                vertexInfo[v].first = vertexInfo[current].first + 1;
                vertexInfo[v].second = vertexInfo[current].second;
            } else if (vertexInfo[v].first == vertexInfo[current].first +1) {
                vertexInfo[v].second += vertexInfo[current].second;
            }
        }
    }

    return vertexInfo[to].second;
}


int main() {
    int verticesCount = 0;
    int edgesCount = 0;

    std::pair<int, int> Edge;

    std::cin >> verticesCount >> edgesCount;

    ListGraph graph(verticesCount);

    for (int i = 0; i < edgesCount; i++) {
        std::cin >> Edge.first >> Edge.second;
        graph.AddEdge(Edge.first, Edge.second);
        graph.AddEdge(Edge.second, Edge.first);
    }

    int from = 0;
    int to = 0;

    std::cin >> from >> to;

    std::cout << taskResult(graph, from, to) << std::endl;

    return 0;
}
