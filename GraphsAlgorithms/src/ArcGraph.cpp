#include <ArcGraph.h>
#include <cassert>

ArcGraph::ArcGraph(int nodesAmount) : _nodesAmount(nodesAmount) {}

ArcGraph::ArcGraph(const IGraph& origin) : ArcGraph(origin.VerticesCount()) {
    for (int from = 0; from < _nodesAmount; from++) {
        std::vector<int> nextVertices = origin.GetNextVertices(from);
        for (int to :nextVertices) {
            assert(to < _nodesAmount);
            AddEdge(from, to);
        }
    }
}

void ArcGraph::AddEdge(int from, int to) {
    assert(from >= 0 && from < _nodesAmount);
    assert(to >= 0 && to < _nodesAmount);
    _pairs.emplace_back(from, to);
}

int ArcGraph::VerticesCount() const {
    return _nodesAmount;
}

std::vector<int> ArcGraph::GetNextVertices(int vertex) const {
    assert(vertex >= 0 && vertex < _nodesAmount);

    std::vector<int> result;
    for (const auto& pair: _pairs) {
        if (pair.first == vertex) {
            result.push_back(pair.second);
        }
    }

    return result;
}

std::vector<int> ArcGraph::GetPrevVertices(int vertex) const {
    assert(vertex >= 0 && vertex < _nodesAmount);

    std::vector<int> result;
    for (const auto& pair: _pairs) {
        if (pair.second == vertex) {
            result.push_back(pair.first);
        }
    }

    return result;
}