#include <ListGraph.h>

ListGraph::ListGraph(int verticesCount) : NodesTable(verticesCount) {}

ListGraph::ListGraph(const IGraph &source) : NodesTable(source.VerticesCount()) {
    for (int i = 0; i < source.VerticesCount(); i++) {
        NodesTable[i] = source.GetNextVertices(i);
    }
}

void ListGraph::AddEdge(int from, int to) {
    if (from < 0 || to < 0) {
        return;
    }

    int verticesCount = NodesTable.size();

    if (from >= verticesCount || to >= verticesCount ) {
        return;
    }

    NodesTable[from].push_back(to);
}

int ListGraph::VerticesCount() const {
    return NodesTable.size();
}

std::vector<int> ListGraph::GetNextVertices(int vertex) const {
    return NodesTable[vertex];
}



std::vector<int> ListGraph::GetPrevVertices(int vertex) const {
    std::vector<int> result = {};

    if (vertex < 0 || vertex >= NodesTable.size()) {
        return result;
    }

    int verticesCount = NodesTable.size();

    for (size_t from = 0; from < verticesCount; from++) {
        int currentVertexPairs = NodesTable[from].size();

        for (size_t i = 0; i < currentVertexPairs; i++) {
            if (NodesTable[from][i] == vertex) {
                result.push_back(from);
            }
        }
    }
}

