#include <MatrixGraph.h>

MatrixGraph::MatrixGraph(int count) : verticesCount(count), adjacencyMatrix(count * count, 0) {}

MatrixGraph::MatrixGraph(const IGraph &source) : MatrixGraph(source.VerticesCount()) {
    for (int i = 0; i < source.VerticesCount(); i++) {
        std::vector<int> fromVertexTo = source.GetNextVertices(i);

        for (auto vertex : fromVertexTo) {
            adjacencyMatrix[i * verticesCount + vertex] = 1;
            // добавить -1 ?
        }
    }
}

void MatrixGraph::AddEdge(int from, int to) {
    int count = verticesCount;

    if (from >= count || to >= count) {
        return;
    }

    adjacencyMatrix[from * count + to] = 1;
}

int MatrixGraph::VerticesCount() const {
    return verticesCount;
}

std::vector<int> MatrixGraph::GetNextVertices(int vertex) const {
    std::vector<int> result = {};

    size_t offset = vertex * verticesCount;

    for (size_t i = 0; i < verticesCount; i++) {
        if (adjacencyMatrix[offset + i] == 1) {
            result.push_back(i);
        }
    }

    return result;
}

std::vector<int> MatrixGraph::GetPrevVertices(int vertex) const {
    std::vector<int> result = {};

    size_t offset = 0;

    for (size_t i = 0; i < verticesCount; i++) {
        if (adjacencyMatrix[offset + vertex] == 1) {
            result.push_back(i);
            offset += verticesCount;
        }
    }

    return result;
}