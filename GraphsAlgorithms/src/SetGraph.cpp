#include <cassert>
#include <SetGraph.h>

SetGraph::SetGraph(int nodesAmount) : _graph(nodesAmount) {}

SetGraph::SetGraph(const IGraph& origin) : SetGraph(origin.VerticesCount()) {
    for (int from = 0; from < origin.VerticesCount(); from++) {
        std::vector<int> nextVertices = origin.GetNextVertices(from);

        for (int to: nextVertices) {
            AddEdge(from, to);
        }
    }
}

void SetGraph::AddEdge(int from, int to) {
    assert(from < _graph.size());
    assert(to < _graph.size());
    _graph[from].insert(to);
}

int SetGraph::VerticesCount() const {
    return _graph.size();
}

std::vector<int> SetGraph::GetNextVertices(int from) const {
    assert(from < _graph.size());
    std::vector<int> result(_graph[from].size());
    std::copy(_graph[from].begin(), _graph[from].end(), result.begin());
    return result;
}

std::vector<int> SetGraph::GetPrevVertices(int vertex) const {
    assert(vertex < _graph.size());

    std::vector<int> result;
    for (int from = 0; from < _graph.size(); from++) {
        for (int to: _graph[from]) {
            if (to == vertex) {
                result.push_back(from);
                break;
            }
        }
    }

    return result;
}