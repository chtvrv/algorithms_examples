#ifndef INCLUDE_LIST_GRAPH_H_
#define INCLUDE_LIST_GRAPH_H_

#include <IGraph.h>

class ListGraph : public IGraph {
 public:
    explicit ListGraph(int verticesCount);
    explicit ListGraph(const IGraph &source);

    ~ListGraph() override = default;

    void AddEdge(int from, int to) override;
    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;
    std::vector<int> GetPrevVertices(int vertex) const override;

 private:
    std::vector<std::vector<int>> NodesTable;
};


#endif  // INCLUDE_LIST_GRAPH_H_