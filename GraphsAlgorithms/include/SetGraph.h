#ifndef INCLUDE_SET_GRAPH_H_
#define INCLUDE_SET_GRAPH_H_

#include <IGraph.h>
#include <vector>
#include <unordered_set>

class SetGraph : public IGraph {
public:
    explicit SetGraph(int nodesAmount);
    explicit SetGraph(const IGraph& origin);
    ~SetGraph() override = default;

    void AddEdge(int from, int to) override;

    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;
    std::vector<int> GetPrevVertices(int vertex) const override;

private:
    std::vector<std::unordered_set<int>> _graph;
};


#endif  // INCLUDE_SET_GRAPH_H_
