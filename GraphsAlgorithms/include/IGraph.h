#ifndef INCLUDE_IGRAPH_H_
#define INCLUDE_IGRAPH_H_

#include <vector>

struct IGraph {
    virtual ~IGraph() = default;

    virtual void AddEdge(int from, int to) = 0;
    virtual int VerticesCount() const = 0;

    virtual std::vector<int> GetNextVertices(int vertex) const = 0;
    virtual std::vector<int> GetPrevVertices(int vertex) const = 0;
};

#endif  // INCLUDE_IGRAPH_H_