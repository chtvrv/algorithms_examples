#ifndef INCLUDE_ARC_GRAPH_H_
#define INCLUDE_ARC_GRAPH_H_

#include <vector>

#include "IGraph.h"


class ArcGraph: public IGraph {
public:
    explicit ArcGraph(int nodesAmount);
    explicit ArcGraph(const IGraph& origin);
    ~ArcGraph() override = default;

    void AddEdge(int from, int to) override;

    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;
    std::vector<int> GetPrevVertices(int vertex) const override;

private:
    std::vector<std::pair<int, int>> _pairs;
    int _nodesAmount;
};


#endif  // INCLUDE_ARC_GRAPH_H_
