#ifndef INCLUDE_MATRIX_GRAPH_H_
#define INCLUDE_MATRIX_GRAPH_H_

#include <IGraph.h>

class MatrixGraph : public IGraph {
 public:
    explicit MatrixGraph(int verticesCount);
    explicit MatrixGraph(const IGraph& origin);

    ~MatrixGraph() override = default;

    void AddEdge(int from, int to) override;
    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;
    std::vector<int> GetPrevVertices(int vertex) const override;

private:
    int verticesCount;
    std::vector<int> adjacencyMatrix;
};

#endif  // INCLUDE_MATRIX_GRAPH_H_