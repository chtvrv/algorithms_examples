#include <iostream>
#include <sstream>

#include <ListGraph.h>
#include <MatrixGraph.h>

int main() {
    ListGraph lG(6);
    MatrixGraph mG(6);

    lG.AddEdge(0, 5);
    lG.AddEdge(0, 4);
    lG.AddEdge(0, 2);
    lG.AddEdge(1, 0);
    lG.AddEdge(1, 2);
    lG.AddEdge(2, 4);
    lG.AddEdge(2, 3);
    lG.AddEdge(3, 1);
    lG.AddEdge(4, 3);
    lG.AddEdge(5, 4);

    mG.AddEdge(0, 5);
    mG.AddEdge(0, 4);
    mG.AddEdge(0, 2);
    mG.AddEdge(1, 0);
    mG.AddEdge(1, 2);
    mG.AddEdge(2, 4);
    mG.AddEdge(2, 3);
    mG.AddEdge(3, 1);
    mG.AddEdge(4, 3);
    mG.AddEdge(5, 4);


    auto printGraph = [](const IGraph &graph, std::ostream &outStream) {
        int verticesCount = graph.VerticesCount();

        for (int i = 0; i < verticesCount; i++) {
            std::vector<int> fromVertex = graph.GetNextVertices(i);

            for (auto x : fromVertex) {
                outStream << i << " -> " << x << '\n';
            }
        }
    };

    std::cout << "ListGraph:" << '\n';
    printGraph(lG, std::cout);

    std::cout << "MatrixGraph:" << '\n';
    printGraph(mG, std::cout);


    return 0;
}
