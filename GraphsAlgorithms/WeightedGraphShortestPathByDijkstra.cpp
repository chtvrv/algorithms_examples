#include <iostream>
#include <vector>
#include <set>
#include <climits>

struct IGraph {
    virtual ~IGraph() = default;

    virtual void AddEdge(int from, int to, int weight) = 0;
    virtual int VerticesCount() const = 0;

    virtual std::vector<int> GetNextVertices(int vertex) const = 0;
    virtual std::vector<int> GetPrevVertices(int vertex) const = 0;
    virtual std::vector<std::pair<int, int>> GetPathsFromVertex(int vertex) const = 0;
};

class ListGraph : public IGraph {
public:
    explicit ListGraph(int verticesCount);
    explicit ListGraph(const IGraph &source);

    ~ListGraph() override = default;

    void AddEdge(int from, int to, int weight) override;
    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;
    std::vector<int> GetPrevVertices(int vertex) const override;

    std::vector<std::pair<int, int>> GetPathsFromVertex(int vertex) const override;

private:
    std::vector<std::vector<std::pair<int, int>>> NodesTable;
};

ListGraph::ListGraph(int verticesCount) : NodesTable(verticesCount) {}

void ListGraph::AddEdge(int from, int to, int weight) {
    if (from < 0 || to < 0) {
        return;
    }

    int verticesCount = NodesTable.size();

    if (from >= verticesCount || to >= verticesCount ) {
        return;
    }

    NodesTable[from].push_back(std::make_pair(to, weight));
}

int ListGraph::VerticesCount() const {
    return NodesTable.size();
}

std::vector<int> ListGraph::GetNextVertices(int vertex) const {
    std::vector<int> result = {};

    for (auto x : NodesTable[vertex]) {
        result.push_back(x.first);
    }

    return result;
}

std::vector<int> ListGraph::GetPrevVertices(int vertex) const {
    std::vector<int> result = {};

    if (vertex < 0 || vertex >= NodesTable.size()) {
        return result;
    }

    int verticesCount = NodesTable.size();

    for (size_t from = 0; from < verticesCount; from++) {
        int currentVertexPairs = NodesTable[from].size();

        for (size_t i = 0; i < currentVertexPairs; i++) {
            if (NodesTable[from][i].first == vertex) {
                result.push_back(from);
            }
        }
    }
}

std::vector<std::pair<int, int>> ListGraph::GetPathsFromVertex(int vertex) const {
    return NodesTable[vertex];
}

int taskResult(const IGraph &graph, int from, int to) {
    std::vector<int> pathToVertex(graph.VerticesCount(), INT_MAX);
    pathToVertex[from] = 0;

    // pair - path to vertex and vertex
    std::set<std::pair<int, int>> priorityQueue;

    priorityQueue.emplace(std::make_pair(0, from));

    while (!priorityQueue.empty()) {
        int currentVertex = (priorityQueue.begin())->second;
        priorityQueue.erase(priorityQueue.begin());

        std::vector<std::pair<int, int>> fromCurrentVertexTo = graph.GetPathsFromVertex(currentVertex);

        for (auto ToAndDistance : fromCurrentVertexTo) {
            // relax process
            if (pathToVertex[ToAndDistance.first] > pathToVertex[currentVertex] + ToAndDistance.second) {
                if (pathToVertex[ToAndDistance.first] != INT_MAX) {
                    priorityQueue.erase(std::make_pair(pathToVertex[ToAndDistance.first], ToAndDistance.first));
                }

                pathToVertex[ToAndDistance.first] = pathToVertex[currentVertex] + ToAndDistance.second;
                priorityQueue.emplace(std::make_pair(pathToVertex[ToAndDistance.first], ToAndDistance.first));
            }
        }
    }

    if (pathToVertex[to] != INT_MAX) {
        return pathToVertex[to];
    } else {
        return -1;
    }
}

int main() {
    int verticesCount = 0;
    int edgesCount = 0;

    std::pair<int, int> Edge;
    int weight = 0;

    std::cin >> verticesCount >> edgesCount;

    ListGraph graph(verticesCount);

    for (int i = 0; i < edgesCount; i++) {
        std::cin >> Edge.first >> Edge.second >> weight;
        graph.AddEdge(Edge.first, Edge.second, weight);
        graph.AddEdge(Edge.second, Edge.first, weight);
    }

    int from = 0;
    int to = 0;

    std::cin >> from >> to;

    std::cout << taskResult(graph, from, to) << std::endl;

    return 0;
}
