/* Реализуйте структуру данных типа “множество строк” на основе динамической хеш-таблицы с открытой адресацией.
Хранимые строки непустые и состоят из строчных латинских букв.
Хеш-функция строки должна быть реализована с помощью вычисления значения многочлена методом Горнера.
Начальный размер таблицы должен быть равным 8-ми.
Перехеширование выполняйте при добавлении элементов в случае, когда коэффициент заполнения таблицы достигает 3/4.
Структура данных должна поддерживать операции добавления строки в множество, удаления строки из множества
и проверки принадлежности данной строки множеству.


1_2. Для разрешения коллизий используйте двойное хеширование.
Требования: В таблице запрещено хранение указателей на описатель элемента.

Формат входных данных:
Каждая строка входных данных задает одну операцию над множеством.
Запись операции состоит из типа операции и следующей за ним через пробел строки, над которой проводится операция.

Тип операции  – один из трех символов:
    +  означает добавление данной строки в множество;
    -  означает удаление  строки из множества;
    ?  означает проверку принадлежности данной строки множеству.
При добавлении элемента в множество НЕ ГАРАНТИРУЕТСЯ, что он отсутствует в этом множестве.
При удалении элемента из множества НЕ ГАРАНТИРУЕТСЯ, что он присутствует в этом множестве.

Формат выходных данных:
Программа должна вывести для каждой операции одну из двух строк OK или FAIL,
в зависимости от того, встречается ли данное слово в нашем множестве. */

#include <iostream>
#include <cassert>
#include <vector>
#include <string>

static const double load_factor = 0.75;

static const char empty_value = 0;
static const char deleted_value = 1;
static const char real_value = 2;

static const int first_prob_seed = 5;
static const int second_prob_seed = 7;

template <typename T, class Hasher>
class HashTable {
 public:
    HashTable(const Hasher& hasher);

    HashTable(const HashTable&) = delete;
    HashTable(const HashTable&&) = delete;

    ~HashTable() = default;

    HashTable& operator=(const HashTable&) = delete;
    HashTable&& operator=(const HashTable&&) = delete;

    bool Add(const T& data);
    bool Find(const T& data);
    bool Delete(const T& data);

 private:
    Hasher hasher;
    std::vector<std::string> table;
    std::vector<char> support;

    int elem_count;

    void ResizeTable();
};

template <typename T, class Hasher>
HashTable<T, Hasher>::HashTable(const Hasher& hasher_) : hasher(hasher_),
    table(8, ""),
    support(8, empty_value),
    elem_count(0) {
}

template <typename T, class Hasher>
bool HashTable<T, Hasher>::Add(const T& data) {
    if ( ((double) elem_count / (double) table.size()) >= load_factor ) {
        ResizeTable();
    }

    int first_deleted_pos = -1;

    int first_hash = hasher(data, first_prob_seed);
    int second_hash = hasher(data, second_prob_seed);

    if (!(second_hash % 2)) {
        second_hash += 1;
    }

    for (size_t i = 0; i < table.size(); i++) {
        int prob_pos = (first_hash + i * second_hash) % table.size();

        if (support[prob_pos] == empty_value) {
            if (first_deleted_pos != -1) {
                table[first_deleted_pos] = data;
                support[first_deleted_pos] = real_value;
                elem_count++;
                return true;
            }

            table[prob_pos] = data;
            support[prob_pos] = real_value;
            elem_count++;
            return true;
        }


        if (support[prob_pos] == deleted_value) {
            if (table[prob_pos] == data) {
                support[prob_pos] = real_value;
                elem_count++;
                return true;
            }

            if (first_deleted_pos == -1) {
                first_deleted_pos = prob_pos;
            }
        }

        if (support[prob_pos] == real_value && table[prob_pos] == data) {
            return false;
        }
    }

    table[first_deleted_pos] = data;
    support[first_deleted_pos] = real_value;
    elem_count++;
    return true;
}

template <typename T, class Hasher>
void HashTable<T, Hasher>::ResizeTable() {
    std::vector<std::string> new_table(table.size() * 2, "");
    std::vector<char> new_support(table.size() * 2, empty_value);

    for (size_t i = 0; i < table.size(); i++) {
        if (support[i] == real_value) {
            std::string data = table[i];

            int first_hash = hasher(data, first_prob_seed);
            int second_hash = hasher(data, second_prob_seed);

            if (!(second_hash % 2)) {
                second_hash += 1;
            }

            for (int j = 0; j < (int) new_table.size(); j++) {
                int prob_pos = (first_hash + j * second_hash) % new_table.size();

                if (new_support[prob_pos] == empty_value) {
                    new_table[prob_pos] = data;
                    new_support[prob_pos] = real_value;
                    break;
                }
            }
        }
    }

    table = std::move(new_table);
    support = std::move(new_support);
}

template <typename T, class Hasher>
bool HashTable<T, Hasher>::Delete(const T& data) {
    int first_hash = hasher(data, first_prob_seed);
    int second_hash = hasher(data, second_prob_seed);

    if (!(second_hash % 2)) {
        second_hash += 1;
    }

    for (size_t i = 0; i < table.size(); i++) {
        int prob_pos = (first_hash + i * second_hash) % table.size();

        if (support[prob_pos] == empty_value) {
            return false;
        }

        if (support[prob_pos] == real_value && table[prob_pos] == data) {
            support[prob_pos] = deleted_value;
            elem_count--;
            return true;
        }
    }

    return false;
}

template <typename T, class Hasher>
bool HashTable<T, Hasher>::Find(const T& data) {
    int first_hash = hasher(data, first_prob_seed);
    int second_hash = hasher(data, second_prob_seed);

    if (!(second_hash % 2)) {
        second_hash += 1;
    }

    for (size_t i = 0; i < table.size(); i++) {
        int prob_pos = (first_hash + i * second_hash) % table.size();

        if (support[prob_pos] == empty_value) {
            return false;
        }

        if (support[prob_pos] == real_value && table[prob_pos] == data) {
            return true;
        }
    }

    return false;
}

class Hasher {
 public:
    int operator()(const std::string& data, int seed) const {
        int hash = 0;

        for (size_t i = 0; i < data.length(); i++) {
            hash = hash * seed + data[i];
        }

        return hash;
    }
};

int main() {
    Hasher h;
    HashTable<std::string, Hasher> table(h);
    char operation = 0;
    std::string word = "";

    while (std::cin >> operation >> word) {
        if (operation == '+') {
            std::cout << (table.Add(word) ? "OK" : "FAIL") << std::endl;
        }

        if (operation == '?') {
            std::cout << (table.Find(word) ? "OK" : "FAIL") << std::endl;
        }

        if (operation == '-') {
            std::cout << (table.Delete(word) ? "OK" : "FAIL") << std::endl;
        }
    }

    return 0;
}
