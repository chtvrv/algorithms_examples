/* Дано число N < 106 и последовательность пар целых чисел из [-231..231] длиной N.
Построить декартово дерево из N узлов, характеризующихся парами чисел {Xi, Yi}.
Каждая пара чисел {Xi, Yi} определяет ключ Xi и приоритет Yi в декартовом дереве.

Добавление узла в декартово дерево выполняйте второй версией алгоритма, рассказанного на лекции:
При добавлении узла (x, y) выполняйте спуск по ключу до узла P с меньшим приоритетом.
Затем разбейте найденное поддерево по ключу x так, чтобы в первом поддереве все ключи меньше x,
а во втором больше или равны x. Получившиеся два дерева сделайте дочерними для нового узла (x, y).
Новый узел вставьте на место узла P.

Построить также наивное дерево поиска по ключам Xi методом из задачи 2.

3_2. Вычислить количество узлов в самом широком слое декартового дерева и количество узлов в самом широком слое
наивного дерева поиска. Вывести их разницу. Разница может быть отрицательна. */

#include <iostream>
#include <vector>
#include <queue>

template <typename node>
int getWidth(node *ptr, int level) {
    if (ptr == nullptr) {
        return 0;
    }

    if (level == 1) {
        return 1;
    }

    return (getWidth(ptr->leftChild, level - 1) + getWidth(ptr->rightChild, level - 1));
}

template <typename node>
int getHeight(node *ptr) {
    node *current = ptr;

    int leftHeight = 0;
    int rightHeight = 0;

    if (current == nullptr) {
        return 0;
    }

    if (current->leftChild) {
        leftHeight = getHeight(current->leftChild);
    }

    if (current->rightChild) {
        rightHeight = getHeight(current->rightChild);
    }

    if (leftHeight > rightHeight) {
        return leftHeight + 1;
    } else {
        return rightHeight + 1;
    }
}

struct TreapNode {
    TreapNode *leftChild;
    TreapNode *rightChild;

    int value;
    int priority;

    TreapNode(TreapNode *l, TreapNode *r, int _value, int _priority) :
        leftChild(l),
        rightChild(r),
        value(_value),
        priority(_priority) {}
};

// запретить присваивание и т.д.

template <class Comparator>
class Treap {
 public:
    explicit Treap(Comparator comp);
    ~Treap();

    void Add(int value, int priority);

    void BreadthFirstSearch(void (*f)(const TreapNode*));

    int getMaxWidth();
 private:
    void Split(TreapNode *currentNode, int keyValue, TreapNode* &left, TreapNode* &right);
    TreapNode* Merge(TreapNode *left, TreapNode *right);

    TreapNode *root;
    Comparator comp;
};

template <class Comparator>
Treap<Comparator>::Treap(Comparator _comp) : root(nullptr), comp(_comp) {}

template <class Comparator>
Treap<Comparator>::~Treap() {
    auto f = [](const TreapNode *node) {delete node;};
    BreadthFirstSearch(f);
}


template <class Comparator>
void Treap<Comparator>::Split(TreapNode *currentNode, int keyValue, TreapNode* &left, TreapNode* &right) {
    if (currentNode == nullptr) {
        left = nullptr;
        right = nullptr;
        return;
    }

    if (currentNode->value <= keyValue) {
        Split(currentNode->rightChild, keyValue, currentNode->rightChild, right);
        left = currentNode;
    } else {
        Split(currentNode->leftChild, keyValue, left, currentNode->leftChild);
        right = currentNode;
    }

    return;
}

template <class Comparator>
TreapNode* Treap<Comparator>::Merge(TreapNode *left, TreapNode *right) {
    if (left == nullptr || right == nullptr) {
        return (left == nullptr) ? right : left;
    }

    if (left->priority > right->priority) {
        left->rightChild = Merge(left->rightChild, right);
        return left;
    } else {
        right->leftChild = Merge(left, right->leftChild);
        return right;
    }
}

template <class Comparator>
void Treap<Comparator>::Add(int value, int priority) {
    auto *newNode = new TreapNode(nullptr, nullptr, value, priority);

    if (root == nullptr) {
        root = newNode;
        return;
    }

    TreapNode *current = root;
    TreapNode *parent = nullptr;

    while (true) {
        if (priority > current->priority) {
            break;
        }

        if (comp(value, current->value)) {
            if (current->leftChild == nullptr) {
                current->leftChild = newNode;
                return;
            }

            parent = current;
            current = current->leftChild;

        } else {
            if (current->rightChild == nullptr) {
                current->rightChild = newNode;
                return;
            }
            parent = current;
            current = current->rightChild;
        }
    }

    TreapNode *leftNode = nullptr;
    TreapNode *rightNode = nullptr;

    Split(current, value, leftNode, rightNode);

    newNode->leftChild = leftNode;
    newNode->rightChild = rightNode;

    bool nodeIsNewRoot = false;

    if (parent == nullptr) {
        root = newNode;
        nodeIsNewRoot = true;
    }

    if (!nodeIsNewRoot) {
        if (comp(newNode->value, parent->value)) {
            parent->leftChild = newNode;
        } else {
            parent->rightChild = newNode;
        }
    }

    return;
}

template <class Comparator>
void Treap<Comparator>::BreadthFirstSearch(void (*f)(const TreapNode*)) {
    if (root == nullptr) {
        return;
    }

    std::queue<TreapNode*> queue = {};

    queue.push(root);

    while (!queue.empty()) {
        TreapNode *node = queue.front();
        queue.pop();

        if (node->leftChild != nullptr) {
            queue.push(node->leftChild);
        }

        if (node->rightChild != nullptr) {
            queue.push(node->rightChild);
        }

        if (f != nullptr) {
            f(node);
        }
    }
    return;
}

template <class Comparator>
int Treap<Comparator>::getMaxWidth() {
    int maxWidth = 0;
    int height = getHeight<TreapNode>(root);

    for (int i = 0; i < height; i++) {
        int width = getWidth<TreapNode>(root, i);

        if (width > maxWidth) {
            maxWidth = width;
        }
    }
    return maxWidth;
}



struct BinaryTreeNode {
    BinaryTreeNode *leftChild;
    BinaryTreeNode *rightChild;
    BinaryTreeNode *parent;

    int value;

    BinaryTreeNode(BinaryTreeNode *left, BinaryTreeNode *right, BinaryTreeNode *_parent, int _value) :
            leftChild(left),
            rightChild(right),
            parent(_parent),
            value(_value) {}
};

template <class Comparator>
class BinaryTree {
public:
    explicit BinaryTree(Comparator comp);
    ~BinaryTree();

    int getMaxWidth();

    void Add(int value);
    void BreadthFirstSearch(void (*f)(const BinaryTreeNode*));

private:
    BinaryTreeNode *root;
    Comparator comp;
};

template <class Comparator>
BinaryTree<Comparator>::BinaryTree(Comparator _comp) : root(nullptr), comp(_comp) {}

template <class Comparator>
BinaryTree<Comparator>::~BinaryTree() {
    auto f = [](const BinaryTreeNode *node) {delete node;};
    BreadthFirstSearch(f);
}

template <class Comparator>
int BinaryTree<Comparator>::getMaxWidth() {
    int maxWidth = 0;
    int height = getHeight<BinaryTreeNode>(root);

    for (int i = 0; i < height; i++) {
        int width = getWidth<BinaryTreeNode>(root, i);

        if (width > maxWidth) {
            maxWidth = width;
        }
    }
    return maxWidth;
}

template <class Comparator>
void BinaryTree<Comparator>::Add(int value) {
    auto *newNode = new BinaryTreeNode(nullptr, nullptr, nullptr, value);

    if (root == nullptr) {
        root = newNode;
        return;
    }

    BinaryTreeNode *current = root;

    while (true) {
        if (comp(value, current->value)) {
            if (current->leftChild == nullptr) {
                current->leftChild = newNode;
                newNode->parent = current;
                break;
            }

            current = current->leftChild;
        }

        if (!comp(value, current->value)) {
            if (current->rightChild == nullptr) {
                current->rightChild = newNode;
                newNode->parent = current;
                break;
            }

            current = current->rightChild;
        }
    }

    return;
}

template <class Comparator>
void BinaryTree<Comparator>::BreadthFirstSearch(void (*f)(const BinaryTreeNode*)) {
    if (root == nullptr) {
        return;
    }

    std::queue<BinaryTreeNode*> queue = {};

    queue.push(root);

    while (!queue.empty()) {
        BinaryTreeNode *node = queue.front();
        queue.pop();

        if (node->leftChild != nullptr) {
            queue.push(node->leftChild);
        }

        if (node->rightChild != nullptr) {
            queue.push(node->rightChild);
        }

        if (f != nullptr) {
            f(node);
        }
    }
    return;
}

class Comp {
public:
    bool operator()(const int &l, const int &r) {
        return l < r;
    }
};

int main() {
    int elemCount = 0;

    int value = 0;
    int priority = 0;

    Comp lessComp;

    Treap<Comp> treap(lessComp);
    BinaryTree<Comp> binTree(lessComp);

    std::cin >> elemCount;

    for (int i = 0; i < elemCount; i++) {
        std::cin >> value >> priority;
        treap.Add(value, priority);
        binTree.Add(value);
    }

    int resultBin = binTree.getMaxWidth();
    int resultTreap = treap.getMaxWidth();

    int taskResult = resultTreap - resultBin;
    std::cout << taskResult << std::endl;

    return 0;
}
