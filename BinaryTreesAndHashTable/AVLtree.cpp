/* Требование для всех вариантов Задачи 4
Решение должно поддерживать передачу функции сравнения снаружи.

4_1. Солдаты. В одной военной части решили построить в одну шеренгу по росту. Т.к. часть была далеко не образцовая,
то солдаты часто приходили не вовремя, а то их и вовсе приходилось выгонять из шеренги за плохо начищенные сапоги.
Однако солдаты в процессе прихода и ухода должны были всегда быть выстроены по росту – сначала самые высокие,
а в конце – самые низкие.
За расстановку солдат отвечал прапорщик, который заметил интересную особенность – все солдаты в части разного роста.
Ваша задача состоит в том, чтобы помочь прапорщику правильно расставлять солдат, а именно для каждого приходящего
солдата указывать, перед каким солдатом в строе он должен становится.

Требования: скорость выполнения команды - O(log n).

Формат входных данных.
Первая строка содержит число N – количество команд (1 ≤ N ≤ 30 000).
В каждой следующей строке содержится описание команды: число 1 и X если солдат приходит в строй (X – рост солдата,
натуральное число до 100 000 включительно) и число 2 и Y если солдата, стоящим в строе на месте Y надо удалить из строя.
Солдаты в строе нумеруются с нуля.
На каждую команду 1 (добавление в строй) вы должны выводить число K – номер позиции,
на которую должен встать этот солдат (все стоящие за ним двигаются назад). */

#include <iostream>
#include <cassert>
#include <queue>

struct AVLtreeNode {
    AVLtreeNode *leftChild;
    AVLtreeNode *rightChild;

    int value;
    int height;
    int nodesCount;

    explicit AVLtreeNode(int _value) : leftChild(nullptr), rightChild(nullptr), value(_value), height(1), nodesCount (1) {}

};

int height(AVLtreeNode *node) {
    if (node != nullptr) {
        return node->height;
    }

    return 0;
}

int nodesSum(AVLtreeNode *node) {
    if (node == nullptr) {
        return 0;
    }

    return node->nodesCount;
}

int position(AVLtreeNode *node, int currentPos, int value);

int balanceFactor(AVLtreeNode *node) {
    assert(node != nullptr);
    return (height(node->rightChild) - height(node->leftChild));
}

void fixHeight(AVLtreeNode *node) {
    int leftHeight = height(node->leftChild);
    int rightHeight = height(node->rightChild);

    if (leftHeight > rightHeight) {
        node->height = leftHeight + 1;
    } else {
        node->height = rightHeight + 1;
    }

    node->nodesCount = nodesSum(node->leftChild) + nodesSum(node->rightChild) + 1;
}

AVLtreeNode* rotateRight(AVLtreeNode *node) {
    AVLtreeNode *fixed = node->leftChild;
    node->leftChild = fixed->rightChild;
    fixed->rightChild = node;

    fixHeight(node);
    fixHeight(fixed);

    return fixed;
}

AVLtreeNode* rotateLeft(AVLtreeNode *node) {
    AVLtreeNode *fixed = node->rightChild;
    node->rightChild = fixed->leftChild;
    fixed->leftChild = node;

    fixHeight(node);
    fixHeight(fixed);

    return fixed;
}

AVLtreeNode* balance(AVLtreeNode *node) {
    fixHeight(node);

    if (balanceFactor(node) == 2) {
        if (balanceFactor(node->rightChild) < 0) {
            node->rightChild = rotateRight(node->rightChild);
        }

        return rotateLeft(node);
    }

    if (balanceFactor(node) == -2) {
        if (balanceFactor(node->leftChild) > 0) {
            node->leftChild = rotateLeft(node->leftChild);
        }

        return rotateRight(node);
    }

    return node;
}

template <class Comparator>
AVLtreeNode* insert(AVLtreeNode *node, int value, int &position, Comparator &comp) {
    if (node == nullptr) {
        return new AVLtreeNode(value);
    }

    if (comp(value, node->value)) {
        node->leftChild = insert<Comparator>(node->leftChild, value, position, comp);
    } else {
        node->rightChild = insert<Comparator>(node->rightChild, value, position, comp);
    }

    return balance(node);
}

AVLtreeNode *findAndRemoveMin(AVLtreeNode* current, AVLtreeNode* &min) {
    if (current->leftChild == nullptr) {
        min = current;
        return current->rightChild;
    }

    current->leftChild = findAndRemoveMin(current->leftChild, min);

    return balance(current);
}

AVLtreeNode *findAndRemoveMax(AVLtreeNode* current, AVLtreeNode* &max) {
    if (current->rightChild == nullptr) {
        max = current;
        return current->leftChild;
    }

    current->rightChild = findAndRemoveMax(current->rightChild, max);

    return balance(current);
}


int count(AVLtreeNode *node) {
    if (node == nullptr) {
        return 0;
    }

    return count(node->leftChild) + count(node->rightChild) + 1;
}

int valueByPosition(AVLtreeNode *node, int position) {
    if (position == nodesSum(node->rightChild)) {
        return node->value;
    }

    if (position < nodesSum(node->rightChild)) {
        return valueByPosition(node->rightChild, position);
    }

    return valueByPosition(node->leftChild, position - nodesSum(node->rightChild) - 1);
}

template <class Comparator>
int position(AVLtreeNode *node, int currentPos, int value, Comparator &comp) {
    if (comp(node->value, value)) {
        return position(node->rightChild, currentPos, value, comp);
    }

    if (comp(value, node->value)) {
        return position(node->leftChild, currentPos + nodesSum(node->rightChild) + 1, value, comp);
    }

    return currentPos + nodesSum(node->rightChild);
}

template <class Comparator>
AVLtreeNode *deleteInNode(AVLtreeNode *current, int value, Comparator comp) {
    if (current == nullptr) {
        return nullptr;
    }

    if (comp(value, current->value)) {
        current->leftChild = deleteInNode(current->leftChild, value, comp);
    } else if (comp(current->value, value)) {
        current->rightChild = deleteInNode(current->rightChild, value, comp);
    } else {
        AVLtreeNode *left = current->leftChild;
        AVLtreeNode *right = current->rightChild;
        delete current;

        if (right == nullptr) {
            return left;
        }

        if (left == nullptr) {
            return right;
        }

        if (right->height > left->height) {
            AVLtreeNode *min = nullptr;
            AVLtreeNode *forInsert = findAndRemoveMin(right, min);
            min->leftChild = left;
            min->rightChild = forInsert;
            return balance(min);
        } else {
            AVLtreeNode *max = nullptr;
            AVLtreeNode *forInsert = findAndRemoveMax(left, max);
            max->rightChild = right;
            max->leftChild = forInsert;
            return balance(max);
        }
    }

    return balance(current);
}

template <class Comparator>
class AVLtree {
 public:
    explicit AVLtree(Comparator &cmp);
    ~AVLtree();

    void Add(int value, int &position);
    void Delete(int value);

    int getPosition(int value);
    int getValueByPosition(int position);

    void BreadthFirstSearch(void (*f)(const AVLtreeNode*));
    void RightLeftMid(AVLtreeNode *node, int position, int &currentPos, int &value);

 private:
    AVLtreeNode *root;
    Comparator comp;
};

template <class Comparator>
AVLtree<Comparator>::AVLtree(Comparator &cmp) : root(nullptr), comp(cmp) {}

template <class Comparator>
AVLtree<Comparator>::~AVLtree() {
    auto f = [](const AVLtreeNode *node) {delete node;};
    BreadthFirstSearch(f);
}

template <class Comparator>
void AVLtree<Comparator>::Add(int value, int &position) {

    root = insert<Comparator>(root, value, position, comp);
}

template <class Comparator>
void AVLtree<Comparator>::Delete(int value) {
    root = deleteInNode(root, value, comp);
}

template <class Comparator>
int AVLtree<Comparator>::getPosition(int value) {
    return position<Comparator>(root, 0, value, comp);
}

template <class Comparator>
void AVLtree<Comparator>::RightLeftMid(AVLtreeNode *node, int position, int &currentPos, int &value) {
    if (node == nullptr) {
        return;
    }

    if (value != 0) {
        return;
    }

    RightLeftMid(node->rightChild, position, currentPos, value);

    currentPos++;

    if (currentPos == position) {
        value = node->value;
        return;
    }

    RightLeftMid(node->leftChild, position, currentPos, value);
}

template <class Comparator>
int AVLtree<Comparator>::getValueByPosition(int position) {
    return valueByPosition(root, position);
}

template <class Comparator>
void AVLtree<Comparator>::BreadthFirstSearch(void (*f)(const AVLtreeNode*)) {
    if (root == nullptr) {
        return;
    }

    std::queue<AVLtreeNode*> queue = {};

    queue.push(root);

    while (!queue.empty()) {
        AVLtreeNode *node = queue.front();
        queue.pop();

        if (node->leftChild != nullptr) {
            queue.push(node->leftChild);
        }

        if (node->rightChild != nullptr) {
            queue.push(node->rightChild);
        }

        if (f != nullptr) {
            f(node);
        }
    }
    return;
}

class Comp {
public:
    bool operator()(const int &l, const int &r) {
        return l < r;
    }
};

int main() {
    int commandsCount = 0;

    int command = 0;
    int arg = 0;
    int value = 0;

    Comp comparator;

    AVLtree<Comp> avl(comparator);

    int position = 0;

    std::cin >> commandsCount;

    for (int i = 0; i < commandsCount; i++) {
        std::cin >> command >> arg;

        switch (command) {
            case 1:
                avl.Add(arg, position);
                position = avl.getPosition(arg);
                std::cout << position << '\n';
                break;

            case 2:
                value = avl.getValueByPosition(arg);
                if (value != -1) {
                    avl.Delete(value);
                }
                break;

            default:
                assert(false);
        }
    }

    return 0;
}