/* Дано число N < 106 и последовательность целых чисел из [-231..231] длиной N.
Требуется построить бинарное дерево, заданное наивным порядком вставки.

Т.е., при добавлении очередного числа K в дерево с корнем root, если root→Key ≤ K,
то узел K добавляется в правое поддерево root; иначе в левое поддерево root.

Требования: Рекурсия запрещена. Решение должно поддерживать передачу функции сравнения снаружи.

2_4. Выведите элементы в порядке level-order (по слоям, “в ширину”). */

#include <iostream>
#include <queue>

struct BinaryTreeNode {
    BinaryTreeNode *leftChild;
    BinaryTreeNode *rightChild;
    BinaryTreeNode *parent;

    int value;

    BinaryTreeNode(BinaryTreeNode *left, BinaryTreeNode *right, BinaryTreeNode *_parent, int _value) :
        leftChild(left),
        rightChild(right),
        parent(_parent),
        value(_value) {}
};

template <class Comparator>
class BinaryTree {
 public:
    explicit BinaryTree(Comparator comp);
    ~BinaryTree();

    void Add(int value);
    void BreadthFirstSearchPrint();
    void BreadthFirstSearchDelete();

 private:
    void BreadthFirstSearch(void (*f)(const BinaryTreeNode&));
    Comparator comp;
    BinaryTreeNode *root;
};

template <class Comparator>
BinaryTree<Comparator>::BinaryTree(Comparator _comp) : root(nullptr), comp(_comp) {}

template <class Comparator>
BinaryTree<Comparator>::~BinaryTree() {
    BreadthFirstSearchDelete();
}

template <class Comparator>
void BinaryTree<Comparator>::Add(int value) {
    auto *newNode = new BinaryTreeNode(nullptr, nullptr, nullptr, value);

    if (root == nullptr) {
        root = newNode;
        return;
    }

    BinaryTreeNode *current = root;

    while (true) {
        if (comp(value, current->value)) {
            if (current->leftChild == nullptr) {
                current->leftChild = newNode;
                newNode->parent = current;
                break;
            }

            current = current->leftChild;
        }

        if (!comp(value, current->value)) {
            if (current->rightChild == nullptr) {
                current->rightChild = newNode;
                newNode->parent = current;
                break;
            }

            current = current->rightChild;
        }
    }

    return;
}


template <class Comparator>
void BinaryTree<Comparator>::BreadthFirstSearch(void (*f)(const BinaryTreeNode&)) {
    if (root == nullptr) {
        return;
    }

    std::queue<BinaryTreeNode*> queue = {};
    queue.push(root);

    while (!queue.empty()) {
        BinaryTreeNode *node = queue.front();
        queue.pop();

        if (node->leftChild != nullptr) {
            queue.push(node->leftChild);
        }

        if (node->rightChild != nullptr) {
            queue.push(node->rightChild);
        }

        f(*node);
    }
    return;
}

template <class Comparator>
void BinaryTree<Comparator>::BreadthFirstSearchPrint() {
    auto f = [](const BinaryTreeNode &node) {std::cout << node.value << ' ';};
    BreadthFirstSearch(f);
}

template <class Comparator>
void BinaryTree<Comparator>::BreadthFirstSearchDelete() {
    auto f = [](const BinaryTreeNode &node) {delete &node;};
    BreadthFirstSearch(f);
}

class Comp {
 public:
    bool operator()(const int &l, const int &r) {
        return l < r;
    }
};

int main() {
    Comp lessComp;
    BinaryTree<Comp> binTree(lessComp);

    int elem_count = 0;
    int elem_value = 0;

    std::cin >> elem_count;

    for (int i = 0; i < elem_count; i++) {
        std::cin >> elem_value;
        binTree.Add(elem_value);
    }

    binTree.BreadthFirstSearchPrint();

    std::cout << std::endl;
    return 0;
}
