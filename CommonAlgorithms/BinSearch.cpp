/* 2_2.
Дан массив целых чисел А[0..n-1].
Известно, что на интервале [0, m] значения массива строго возрастают,
a на интервале [m, n-1] строго убывают. Найти m за O(log m).
Требования:  Время работы O(log m). Внимание! В этой задаче сначала нужно определить диапазон
для бинарного поиска размером порядка m, а потом уже в нем делать бинарный поиск. */

#include <iostream>
#include <assert.h>

int inflection_point(int *buf, int count);

int main() {
    int buf_size = 0;
 
    std::cin >> buf_size;
    assert(buf_size > 0 && buf_size <= 10000);
 
    int *buf = new int[buf_size];
 
    for (int i = 0; i < buf_size; i++) {
        std::cin >> buf[i];
    }
 
    int result = inflection_point(buf, buf_size);
 
    delete[] buf;
 
    std::cout << result << std::endl;
    return 0;
}

int inflection_point(int *buf, int buf_size) {
    int n = buf_size - 1;  // last index
 
    if (buf[n-1] < buf[n]) {
        return n;
    }
 
    if (buf[0] > buf[1]) {
        return 0;
    }
 
    int l_pos = 0;
    int r_pos = n;

    // ищем диапазон
    for (int i = 2; i < n; i *= 2) {
        if (buf[i] > buf[i+1] && buf[i] > buf[i-1]) {
            return i;
        } else if (buf[i] < buf[i+1]) {
            l_pos = i + 1;
        } else {
            r_pos = i - 1;
            break;
        }
    }

    // bin_search
    while (l_pos <= r_pos) {
        int mid = (l_pos + r_pos) / 2;

        if (buf[mid] < buf[mid+1]) {
            l_pos = mid + 1;
        } else if (buf[mid] > buf[mid-1]) {
            return mid;
        } else {
            r_pos = mid - 1;
        }

    }

    return -1;
}
