/* 1_4.
“Считалочка”. В круг выстроено N человек, пронумерованных числами от 1 до N.
Будем исключать каждого k-ого до тех пор, пока не уцелеет только один человек.
(Например, если N=10, k=3, то сначала умрет 3-й, потом 6-й, затем 9-й, затем 2-й, затем 7-й, потом 1-й,
потом 8-й, за ним - 5-й, и потом 10-й. Таким образом, уцелеет 4-й.) Необходимо определить номер уцелевшего.
N, k ≤ 10000.
Требования:  Решить перебором. */

#include <iostream>
#include <assert.h>

struct Node {
    int origin_place; // Первоначальное место в круге
    struct Node *next;

    Node(int pos) : origin_place(pos), next(nullptr) {}
};

typedef struct Node node;

class My_forward_list {
 public:
    My_forward_list();
    ~My_forward_list();

    void Append(int origin_place);
    void Delete_node(int pos);
    void Print();

    int Return_head_value();

    int Get_size();

 private:
    node *head;
    node *tail;

    int list_size;
};

My_forward_list::My_forward_list() : head(nullptr), tail(nullptr), list_size(0) {}

void My_forward_list::Append(int origin_place) {
    list_size++;

    if (head == nullptr && tail == nullptr) {
        head = new node(origin_place);
        tail = head;
        return;
    }

    node *new_node = new node(origin_place);
    tail->next = new_node;
    tail = new_node;
    new_node->next = nullptr;
}

void My_forward_list::Print() {
    node *seek = head;

    while (seek->next != nullptr) {
        std::cout << seek->origin_place << ' ';
        seek = seek->next;
    }

    std::cout << seek->origin_place << std::endl;

    // check for empty


}


void My_forward_list::Delete_node(int pos) {
    assert(pos > 0);
    assert(pos <= list_size);

    if (list_size == 1) {
        return;
    }

    list_size--;

    node *seek = head;
    node *temp = nullptr;

    // Delete head
    if (pos == 1) {
        temp = head->next;
        delete head;
        head = temp;
        return;
    }

    for (int i = 2; i <= pos; i++) {
        temp = seek;
        seek = seek->next;
    }

    temp->next = seek->next;
    delete seek;

}

int My_forward_list::Get_size() {
    return list_size;
}


int My_forward_list::Return_head_value() {
    assert (head != nullptr);
    return head->origin_place; // zamena na value
}


My_forward_list::~My_forward_list() {
    node *seek = head;
    
    while (seek->next != nullptr) {
        node *temp = seek->next;
        delete seek;
        seek = temp;
    }

    delete seek;
}
    
int game_result(int circle_length, int kill_series);

int main() {
    int circle_length = 0;
    int kill_series = 0;

    std::cin >> circle_length;
    std::cin >> kill_series;
    
    assert((circle_length >= 0 && circle_length <= 10000) && (kill_series >= 0 && kill_series <= 10000));

    std::cout << game_result(circle_length, kill_series) << std::endl;

    return 0;
}

int game_result(int circle_length, int kill_series) {
    My_forward_list list;

    for (int i = 1; i <= circle_length; i++) {
        list.Append(i);
    }
 
    int list_size = list.Get_size();
    int live_before_round = list_size;
    int offset = 0;

    while (list_size > kill_series) {
        int kills_in_round = ((list_size + offset) / kill_series);
        int killed = 0;

        live_before_round = list.Get_size(); 

        for (int i = 1; i <= kills_in_round; i++) {
            int dead_pos = -offset - killed + (i * kill_series);
            list.Delete_node(dead_pos);
            killed++;
        }

        offset = (live_before_round + offset) % kill_series;

        list_size = list.Get_size();
    }

    while (list_size != 1) {
        int dead_pos = (kill_series % list_size);

        if (dead_pos == 0) {
            dead_pos = list_size;
        }

        dead_pos -= offset;

        if (dead_pos == 0) {
            dead_pos = list_size;
        }

        if (dead_pos < 0) {
            dead_pos += list_size;
        }

        offset = list_size - dead_pos;

        list.Delete_node(dead_pos);

        list_size = list.Get_size();

    }

    return list.Return_head_value();
}
