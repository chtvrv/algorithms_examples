/* Требование для всех вариантов Задачи 5
Во всех задачах данного раздела необходимо реализовать и использовать сортировку слиянием. 
Решение должно поддерживать передачу функции сравнения снаружи.
Общее время работы алгоритма O(n log n).

5_2. Современники.
Группа людей называется современниками если был такой момент, когда они могли собраться вместе.
Для этого в этот момент каждому из них должно было  уже исполниться 18 лет, но ещё не исполниться 80 лет.
Дан список Жизни Великих Людей. Необходимо получить максимальное количество современников.
В день 18летия человек уже может принимать участие в собраниях, а в день 80летия и в день смерти уже не может.
Замечание. Человек мог не дожить до 18-летия, либо умереть в день 18-летия.
В этих случаях принимать участие в собраниях он не мог. */

#include <iostream>
#include <algorithm>
#include <string.h>
#include <assert.h>

template<class T>
class LessComp {
 public:
    bool operator()(const T& left, const T& right) {
        return left < right;
    }
};

template<class T, class Compare>
void merge(T* left_half, int l_length, T* right_half, int r_length, T* destination, Compare comp) {
    int left_iter = 0;
    int right_iter = 0;
    int dest_iter = 0;

    while ((left_iter < l_length) && (right_iter < r_length)) {
        if (comp(left_half[left_iter], right_half[right_iter])) {
            destination[dest_iter] = left_half[left_iter];
            left_iter++;
        } else {
            destination[dest_iter] = right_half[right_iter];
            right_iter++;
        }

        dest_iter++;
    }

    // Одна из половин уже слита, осталось просто докопировать другую

    size_t moving_part_size = 0;

    if (left_iter == l_length) {
        moving_part_size = sizeof(T) * (r_length - right_iter);
        memcpy(destination + dest_iter, right_half + right_iter, moving_part_size);
    } else {
        moving_part_size = sizeof(T) * (l_length - left_iter);
        memcpy(destination + dest_iter, left_half + left_iter, moving_part_size);
    }

}
        
template<class T, class Compare>
void merge_sort(T* array, int size_in_elem, Compare comp) {

    if (size_in_elem <= 1) {
        return;
    }

    int first_len = size_in_elem / 2;
    int second_len = size_in_elem - first_len;
    
    merge_sort(array, first_len, comp);
    merge_sort(array + first_len, second_len, comp);

    T *buffer = new T[size_in_elem];

    merge(array, first_len, array + first_len, second_len, buffer, comp);

    size_t all_buffer_size = sizeof(T) * size_in_elem;

    memcpy(array, buffer, all_buffer_size);;

    delete[] buffer;

}

struct Member {
    int d_birth;
    int m_birth;
    int y_birth;

    int d_death;
    int m_death;
    int y_death;

};

struct Edge {
    int delta;

    int day;
    int month;
    int year;
};

typedef struct Member member;
typedef struct Edge edge;


bool operator < (const edge& left, const edge& right) {

    if (left.year < right.year) {
        return true;
    }

    if (left.year == right.year) {
        if (left.month < right.month) {
            return true;
        }

        if (left.month == right.month) {
            if (left.day < right.day) {
                return true;
            }


            if (left.day == right.day) {
                if (left.delta < right.delta) {
                    return true;
                }
            }

        }
    }

    return false;
}


int task_result(member *buffer, int buf_size);

bool is_adult(member *memb);

bool make_true_range(member *memb);  // если возможно, преобразует в промежуток, в котором человек может быть совр.

int make_edges(member *buffer, int buf_size, edge *edge_buf);  // разбивает каждого человека на два перехода


bool is_adult(member *memb) {
    const int adulthood = 18;

    if ((memb->y_death - memb->y_birth) < adulthood) {  // 
        return false;
    }
 
    if ((memb->y_death - memb->y_birth) == adulthood) {
        if (memb->m_birth > memb->m_death) {
        return false;
    }
 
        if (memb->m_birth == memb->m_death) {
            if (memb->d_birth >= memb->d_death) {
                return false;
            }
        }
    }

    return true;
}

bool make_true_range(member *memb) {
    const int adulthood = 18;
    const int senility = 80;

    if (!is_adult(memb)) {
        return false;
    }

    if ((memb->y_death - memb->y_birth) > senility) {
        memb->y_death = memb->y_birth + senility;
        memb->m_death = memb->m_birth;
        memb->d_death = memb->d_birth;

        memb->y_birth += adulthood;

        return true;
    }

    if ((memb->y_death - memb->y_birth) == senility) {
        if (memb->m_death > memb->m_birth) {
            memb->m_death = memb->m_birth;
            memb->d_death = memb->d_birth;

            memb->y_birth += adulthood;

            return true;

        }

        if (memb->m_death == memb->m_birth) {
            if (memb->d_death >= memb->d_birth) {
                memb->d_death = memb->d_birth;

                memb->y_birth += adulthood;

                return true;
            }
        }
    }

    memb->y_birth += adulthood;

    return true;
}
    
int make_edges(member *buffer, int buf_size, edge *edge_buf) {  // разбивает каждого человека на два перехода
    int edge_count = 0;

    for (int i = 0; i < buf_size; i++) {

        if (make_true_range(&buffer[i])) {

            edge birth_edge = { 1, buffer[i].d_birth, buffer[i].m_birth, buffer[i].y_birth };

            edge_buf[edge_count] = birth_edge;

            edge_count++;

            edge death_edge = { -1, buffer[i].d_death, buffer[i].m_death, buffer[i].y_death };
            
            edge_buf[edge_count] = death_edge;

            edge_count++;
        }
    }

    return edge_count;
}


int main() {

    int buf_size = 0;
  
    std::cin >> buf_size;
    assert(buf_size > 0);
 
    member *memb_array = new member[buf_size];
 
    for (int i = 0; i < buf_size; i++) {
        std::cin >> memb_array[i].d_birth >> memb_array[i].m_birth >> memb_array[i].y_birth;
        std::cin >> memb_array[i].d_death >> memb_array[i].m_death >> memb_array[i].y_death;
    }
 
    int result = task_result(memb_array, buf_size);
 
    std::cout << result << std::endl;

    delete[] memb_array;

    return 0;
}

int task_result(member *memb_array, int buf_size) {
    edge *full_edge_buf = new edge[buf_size * 2];
    
    int true_edge_buf_size = make_edges(memb_array, buf_size, full_edge_buf);

    edge *true_edge_buf = new edge[true_edge_buf_size];                                       
                                                                                                               
    memcpy(true_edge_buf, full_edge_buf, sizeof(edge) * true_edge_buf_size);

    delete[] full_edge_buf;                                                                                   
                                                                                                               
    LessComp<edge> cmp;
                                                                                                           
    merge_sort(true_edge_buf, true_edge_buf_size, cmp);

    int max_result = 0;
    int result = 0;

    for (int i = 0; i < true_edge_buf_size; i++) {
        result += true_edge_buf[i].delta;

        if (result > max_result) {
            max_result = result;
        }

    }

    delete[] true_edge_buf;   

    return max_result;

}
