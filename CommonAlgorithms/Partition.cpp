/* Даны неотрицательные целые числа n,k и массив целых чисел из [0..10^9] размера n.
Требуется найти k-ю порядковую статистику. т.е. напечатать число, которое бы стояло
на позиции с индексом k (0..n-1) в отсортированном массиве. 

Требования: к дополнительной памяти: O(n). Среднее время работы: O(n).
Должна быть отдельно выделенная функция partition. Рекурсия запрещена.
Решение должно поддерживать передачу функции сравнения снаружи.

Функцию Partition следует реализовывать методом прохода двумя итераторами в одном направлении.

Описание для случая прохода от начала массива к концу:

Выбирается опорный элемент. Опорный элемент меняется с последним элементом массива.
Во время работы Partition в начале массива содержатся элементы, не бОльшие опорного.
Затем располагаются элементы, строго бОльшие опорного. В конце массива лежат нерассмотренные элементы. Последним элементом лежит опорный.
Итератор (индекс) i указывает на начало группы элементов, строго бОльших опорного.
Итератор j больше i, итератор j указывает на первый нерассмотренный элемент.
Шаг алгоритма. Рассматривается элемент, на который указывает j. Если он больше опорного, то сдвигаем j.
Если он не больше опорного, то меняем a[i] и a[j] местами, сдвигаем i и сдвигаем j.
В конце работы алгоритма меняем опорный и элемент, на который указывает итератор i. */

#include <iostream>
#include <assert.h>

uint32_t xor128(void) {  // генератор псевдо-рандомных чисел
  static uint32_t x = 123456789;
  static uint32_t y = 362436069;
  static uint32_t z = 521288629;
  static uint32_t w = 88675123;
  uint32_t t;
  t = x ^ (x << 11);   
  x = y; y = z; z = w;   
  return w = w ^ (w >> 19) ^ (t ^ (t >> 8));
}

template <class Compare>
int partition(int *array, int buf_size, Compare comp) {
    int n = buf_size;

    if (n <= 1) {
        return 0;
    }

    int pivot_random_index = xor128() % n;
    
    const int pivot = array[pivot_random_index];
    
    std::swap(array[pivot_random_index], array[0]);

    int i = n - 1;
    int j = n - 1;

    while (i >= 1) {
        if (!comp(array[i], pivot)) {
            std::swap(array[i], array[j]);
            i--;
            j--;
        } else {
            i--;
        }
    }

    std::swap(array[j], array[0]);

    return j;
}

template <class Compare>
int ordinal_stat(int *array, int buf_size, int k, Compare comp) { 
    int left_edge = 0;
    int right_edge = buf_size;

    int k_pos = partition(array + left_edge, right_edge - left_edge, comp);

    while (k_pos != k) {
        if (k_pos > k) {
            right_edge = k_pos;

            k_pos = left_edge + partition(array + left_edge, right_edge - left_edge, comp);
    
        } else {
            left_edge = k_pos + 1;

            k_pos += partition(array + left_edge, right_edge - left_edge, comp) + 1;
        }
    }

    return array[k_pos];
}

template <class T>
class LessComp {
 public:
    bool operator() (const T& l, const T& r) {
        return l < r;
    }
};


int main() {
    int buf_size = 0;
    int k = 0;

    std::cin >> buf_size;
    assert(buf_size > 0);

    std::cin >> k;

    assert(k >= 0);

    int *buffer = new int[buf_size];

    for (int i = 0; i < buf_size; i++) {
        std::cin >> buffer[i];
    }

    LessComp<int> cmp;

    int result = ordinal_stat(buffer, buf_size, k, cmp);

    std::cout << result << std::endl;

    delete[] buffer;

    return 0;
}
