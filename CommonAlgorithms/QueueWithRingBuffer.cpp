/* Во всех задачах из следующего списка следует написать структуру данных,обрабатывающую команды push* и pop*

Формат входных данных.
В первой строке количество команд n. n ≤ 1000000.
Каждая команда задаётся как 2 целых числа: a b.
a = 1 - push front
a = 2 - pop front
a = 3 - push back
a = 4 - pop back

Команды добавления элемента 1 и 3 заданы с неотрицательным параметром b.
Для очереди используются команды 2 и 3. Для дека используются все четыре команды.
Если дана команда pop*, то число b - ожидаемое значение.
Если команда pop вызвана для пустой структуры данных, то ожидается “-1”. 
Формат выходных данных.
Требуется напечатать YES - если все ожидаемые значения совпали. 
Иначе, если хотя бы одно ожидание не оправдалось, то напечатать NO

3_1. 
Реализовать очередь с динамическим зацикленным буфером.
Требования: Очередь должна быть реализована в виде класса. */

#include <iostream>
#include <assert.h>
#include <sstream>
#include <string.h>

class Queue {
 public:
    Queue(int capacity);
    ~Queue();

    void Push(int value);
    int Pop();

    bool Is_empty();
    bool Is_full();

 private:
    void Realloc();

    int *buffer;

    int head;
    int tail;

    int capacity;
    int size;
};

Queue::Queue(int cap = 10) : capacity(cap), head(0), tail(0), size(0) {
    buffer = new int[capacity];
}

Queue::~Queue() {
    delete[] buffer;
}

bool Queue::Is_empty() {
    if (size == 0) {
        return true;
    }

    return false;
}

bool Queue::Is_full() {
    if (size == capacity) {
        return true;
    }

    return false;
}

void Queue::Realloc() {
    int *temp = new int[capacity * 2];
    //check
    
    size_t right_part_size = sizeof(int) * (capacity - head);

    memcpy(temp, (buffer + head), right_part_size);
    size_t left_part_size = 0;

    if (tail != 0) {
        left_part_size = sizeof(int) * (tail);
        memcpy(temp + (capacity - head), (buffer), left_part_size);
    }

    capacity *= 2;
    delete[] buffer;
    buffer = temp;
    head = 0;
    tail = size;
}


void Queue::Push(int value) {
    if (Is_full()) {
        Realloc();
    }

    buffer[tail] = value;
    size++;
    tail = (tail + 1) % capacity;
}

int Queue::Pop() {
    assert( !Is_empty() );

    int temp = head;
    size--;
    head = (head + 1) % capacity;
    return buffer[temp];
}


void run(std::istream& input, std::ostream& output) {
    Queue queue;
    
    int n = 0;
    input >> n;

    bool result = true;

    for (int i = 0; i < n; i++) {
        int command = 0;
        int data = 0;

        input >> command >> data;

        switch (command) {
            case 2:
                if (queue.Is_empty()) {
                    result = result && data == -1;
                } else {
                    result = result && data == queue.Pop();
                }
                
                break;

            case 3:
                queue.Push(data);
                break;

            default:
                assert(false);

        }
    }

    if (result) {
        output << ("YES");
    } else {
        output << ("NO");
    }
}

void test_queue() {
    std::stringstream input;
	std::stringstream output;

	int count = 10000;
	input << count * 2 << std::endl;
	for( int i = 0; i < count; i++ )
		input << "3 " << i << std::endl;
	for( int i = 0; i < count; i++ )
		input << "2 " << i << std::endl;
	run(input, output);
	assert( output.str() == "YES" );
}


int main() {
    run(std::cin, std::cout);
    std::cout << std::endl;

    return 0;
}
