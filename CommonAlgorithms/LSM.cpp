/* 7_2.
LSD для long long.
Дан массив неотрицательных целых 64-битных чисел. Количество чисел не больше 106.
Отсортировать массив методом поразрядной сортировки LSD по байтам. */


#include <iostream>
#include <assert.h>
#include <string.h>

static const int bits_in_value = 64;
static const int bits_in_byte = 8;
static const int bytes_in_value = bits_in_value / bits_in_byte;

static const int base = (1 << bits_in_byte);

int get_byte(long long value, int byte_pos) {
    int moving_bits_count = bits_in_byte * (bytes_in_value - byte_pos - 1);

    int bit_mask = base - 1;
 
    int byte_value = (value >> moving_bits_count) & bit_mask;
 
    return byte_value;
}

void LSD_sort(long long *array, int buf_size) {
    int *bytes_buf = new int[base];
    long long *temp = new long long[buf_size];

    for (int i = (bytes_in_value - 1); i >= 0; i--) {
        for (int j = 0; j < base; j++) {
            bytes_buf[j] = 0;
        }

        for (int j = 0; j < buf_size; j++) {
            int byte_value = get_byte(array[j], i);

            bytes_buf[byte_value] += 1;
        }

        for (int j = 1; j < base; j++) {
            bytes_buf[j] += bytes_buf[j - 1];
        }

        for (int j = buf_size - 1; j >= 0; j--) {
            int byte_value = get_byte(array[j], i);

            temp[--bytes_buf[byte_value]] = array[j];
        }

        memcpy(array, temp, sizeof(long long) * buf_size);
    }

    delete[] bytes_buf;
    delete[] temp;
}


int main() {
 
    int length = 0;

    std::cin >> length;

    assert(length > 0);


    long long *buf = new long long[length];

    for (int i = 0; i < length; i++) {
        std::cin >> buf[i];
    }

    LSD_sort(buf, length);

    for (int i = 0; i < length; i++) {
        std::cout << buf[i] << ' ';
    }

    std::cout << std::endl;

    delete[] buf;

    return 0;
}

