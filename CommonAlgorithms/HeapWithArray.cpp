/* Требование для всех вариантов Задачи 4
Решение всех задач данного раздела предполагает использование кучи, реализованной в виде класса.
Решение должно поддерживать передачу функции сравнения снаружи.

4_2. Быстрое сложение.
Для сложения чисел используется старый компьютер.
Время, затрачиваемое на нахождение суммы двух чисел равно их сумме.
Таким образом для нахождения суммы чисел 1,2,3 может потребоваться разное время,
в зависимости от порядка вычислений.

((1+2)+3) -> 1+2 + 3+3 = 9
((1+3)+2) -> 1+3 + 4+2 = 10
((2+3)+1) -> 2+3 + 5+1 = 11

Требуется написать программу, которая определяет минимальное время,
достаточное для вычисления суммы заданного набора чисел.
Формат входных данных. Вначале вводится n - количество чисел. Затем вводится n строк - значения чисел
(значение каждого числа не превосходит 10^9, сумма всех чисел не превосходит 2*10^9).
Формат выходных данных. Натуральное число - минимальное время. */

//class !!!! Поддержка внешней функции сравнения 

#include <iostream>
#include <assert.h>
#include <string.h>
#include <algorithm>

class My_array {  // Ограниченный функционал
 public:
    My_array(int);
    My_array(const int *buf, int size);
    ~My_array();

    int& operator[](int);

    void Insert(int);
    void Delete_last();

    int Size();
    int Last();

    bool Is_empty();

 private:
    int *buffer;

    void Realloc();

    int size;
    int capacity;
};

My_array::My_array(int cap = 16) : capacity(cap) {
    buffer = new int[capacity];
}

My_array::My_array(const int *buf, int siz) : size(siz), capacity(16) {
    assert(buf != nullptr);

    while (capacity <= size) {
        capacity *= 2;
    }

    buffer = new int[capacity];

    memcpy(buffer, buf, sizeof(int) * size);
}

My_array::~My_array() {
    delete [] buffer;
}

void My_array::Insert(int value) {
    if (size == capacity) {
        Realloc();
    }

    buffer[size] = value;
    size++;
}

int My_array::Size() {
    return size;
}

int My_array::Last() {
    return buffer[size - 1];
}

void My_array::Delete_last() {
    assert(size);
    size--;
}

bool My_array::Is_empty() {
    if (size == 0) {
        return true;
    }

    return false;
}

int& My_array::operator[](int index) {
    assert(index >= 0 && index <= (size -1));

    return buffer[index];
}

void My_array::Realloc() {
    int *temp = new int[capacity * 2];

    size_t moving_part_size = sizeof(int) * capacity;

    memcpy(temp, buffer, moving_part_size);

    capacity *= 2;
    delete[] buffer;
    buffer = temp;
}

class LessComp {
 public:
    bool operator()(const int& l, const int& r) {
        return l < r;
    }
};

class MoreComp {
 public:
    bool operator()(const int& l, const int& r) {
        return l > r;
    }
};
    

template <class Comparator>
class Heap {
 public:
    Heap() = default;

    Heap(const int *buf, int count);

    // Построение из массива поддержать
    ~Heap() = default;

    void Insert(int element);

    int Size();

    int Extract_top();

 private:
    My_array array;

    void Build_heap();
    void Sift_down(int i);
    void Sift_up(int i);

    Comparator comp;
};

template <class Comparator> 
Heap<Comparator>::Heap(const int *buf, int count) : array(buf, count) {
    Build_heap();
}


template <class Comparator>
void Heap<Comparator>::Sift_down(int index) {
    int left = 2 * index + 1;
    int right = 2 * index + 2;

    int minimal = index;

    if (left < array.Size() && comp(array[left], array[minimal])) {
        minimal = left;
    }

    if (right < array.Size() && comp(array[right], array[minimal])) {
        minimal = right;
    }

    if (minimal != index) {
        std::swap(array[index], array[minimal]);
        Sift_down(minimal);
    }
}

template <class Comparator>
void Heap<Comparator>::Sift_up(int index) {
    while (index > 0) {
        int parent = (index - 1) / 2;

        if (!comp(array[index], array[parent])) {
            return;
        }

        std::swap(array[index], array[parent]);
        index = parent;
    }
}

template <class Comparator>
void Heap<Comparator>::Build_heap() {
    for (int i = array.Size() / 2 - 1; i >= 0; i--) {
        Sift_down(i);
    }
}

template <class Comparator>
int Heap<Comparator>::Size() {
    return array.Size();
}
    

template <class Comparator>
void Heap<Comparator>::Insert(int element) {
    array.Insert(element);
    Sift_up(array.Size() - 1);
}

template <class Comparator>
int Heap<Comparator>::Extract_top() {
    assert(!array.Is_empty());
    
    int result = array[0];

    array[0] = array.Last();
    array.Delete_last();

    if (!array.Is_empty()) {
        Sift_down(0);
    }

    return result;
}

int quick_sum(int *buf, int count);

int main() {
    int elem_count = 0;
    int elem_value = 0;

    std::cin >> elem_count;
    assert(elem_count >= 0);

    int *buffer = new int[elem_count];

    for (int i = 0; i < elem_count; i++) {
        std::cin >> buffer[i];
    }

    std::cout << quick_sum(buffer, elem_count) << std::endl;

    delete[] buffer;

    return 0;
}

int quick_sum(int *buf, int count) {
    if (count < 1) {
        return 0;
    }

    Heap<LessComp> heap_min(buf, count);

    int result_time = 0;

    while (heap_min.Size() != 1) {
        int A_num = heap_min.Extract_top();
        int B_num = heap_min.Extract_top();

        result_time += A_num + B_num;

        heap_min.Insert(A_num + B_num);
    }

    return result_time;

}
