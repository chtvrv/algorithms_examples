#include "5_2.h"
#include <sstream>
#include <gtest/gtest.h>

int run(std::stringstream& input) {

    int buf_size = 0;

    input >> buf_size;
    assert(buf_size > 0);
 
    member *memb_array = new member[buf_size];
 
    for (int i = 0; i < buf_size; i++) {
        input >> memb_array[i].d_birth >> memb_array[i].m_birth >> memb_array[i].y_birth;
        input >> memb_array[i].d_death >> memb_array[i].m_death >> memb_array[i].y_death;
    }
 
    int result = task_result(memb_array, buf_size);

    delete[] memb_array;

    return result;
}

TEST(task_result, first_context_test) {
    std::stringstream input;

    input << "3\n" <<  // тест с контеста
        "2 5 1980 13 11 2055 " <<
        "1 1 1982 1 1 2030 " <<
        "2 1 1920 2 1 2000 ";

    int result = run(input);
 
    EXPECT_EQ(result, 3);
}

TEST(task_result, first_manual_test) {  // ручной пример на 4
    std::stringstream input;

    input << "6\n" <<
        "1 1 1920 1 1 1960 " <<
        "1 1 1950 1 1 1980 " <<
        "1 1 1970 1 1 2000 " <<
        "1 1 1981 1 1 2050 " <<
        "1 1 1979 1 1 2060 " <<
        "1 1 1940 1 1 2020 ";
    int result = run(input);
 
    EXPECT_EQ(result, 4);
}

TEST(task_result, second_manual_test) {  // ручной пример на 3
    std::stringstream input;

    input << "4\n" <<
        "4 4 2060 4 4 2120 " <<
        "5 5 2070 6 5 2088 " <<
        "6 6 2055 6 6 2155 " <<
        "7 7 2056 8 7 2084 ";
    int result = run(input);
 
    EXPECT_EQ(result, 3);
}

TEST(task_result, sum_manual_test) {  // последовательность на 3, затем на 4, убедимся что найдет поздний 
    std::stringstream input;

    input << "10\n" <<
        "4 4 2060 4 4 2120 " <<
        "5 5 2070 6 5 2088 " <<
        "6 6 2055 6 6 2155 " <<
        "7 7 2056 8 7 2084 " <<
        "1 1 1920 1 1 1960 " <<
        "1 1 1950 1 1 1980 " <<
        "1 1 1970 1 1 2000 " <<
        "1 1 1981 1 1 2050 " <<
        "1 1 1979 1 1 2060 " <<
        "1 1 1940 1 1 2020 ";
    int result = run(input);
 
    EXPECT_EQ(result, 4);
}

TEST(task_result, not_18) {  // все несовершеннолетние, ожидаем 0
    std::stringstream input;

    input << "3\n" <<
        "10 10 1990 15 12 2006 " <<
        "8 8 1992 7 8 2010 " <<
        "3 3 1990 2 3 2006 ";

    int result = run(input);

    EXPECT_EQ(result, 0);
}

TEST(task_result, one_18) {  // один несовершеннолетний (не в день смерти), ожидаем 1
    std::stringstream input;

    input << "3\n" <<
        "10 10 1990 15 12 2006 " <<
        "8 8 1992 9 8 2010 " <<  // !
        "3 3 1990 2 3 2006 ";

    int result = run(input);

    EXPECT_EQ(result, 1);
}

TEST(task_result, death_in_18) {  // один несовершеннолетний (день смерти), ожидаем 0
    std::stringstream input;

    input << "3\n" <<
        "10 10 1990 15 12 2006 " <<
        "8 8 1992 8 8 2010 " <<  // !
        "3 3 1990 2 3 2006 ";

    int result = run(input);

    EXPECT_EQ(result, 0);
}

TEST(task_result, old) {  // человек жил во время с другими, но был уже слишком старый
    std::stringstream input;

    input << "4\n" <<
        "1 1 100 10 10 10000 " <<
        "15 15 1940 20 12 2000 " <<
        "15 15 1941 21 12 2000 " <<
        "15 15 1942 22 12 2000";

    int result = run(input);

    EXPECT_EQ(result, 3);

}

TEST(task_result, friend_test) {  // не пересекаются
    std::stringstream input;

    input << "2\n" <<
        "1 1 1 1 1 20 " <<
        "1 1 3 1 1 22 ";

    int result = run(input);

    EXPECT_EQ(result, 1);

}

TEST(task_result, death) {  // человек мог увидеть только в день своей смерти
    std::stringstream input;

    input << "2\n" <<
        "1 1 1 1 1 20 " <<
        "1 1 2 1 1 21 ";

    int result = run(input);

    EXPECT_EQ(result, 1);
}

TEST(task_result, before_death) {  // человек увидел за день до своей смерти
    std::stringstream input;

    input << "2\n" <<
        "1 1 1 2 1 20 " <<
        "1 1 2 1 1 21 ";

    int result = run(input);

    EXPECT_EQ(result, 2);
}

TEST(task_result, cross) {  // современнство 1 и 2, 2 и 3, но не 1 с 3, ждём 2
    std::stringstream input;

    input << "3\n" <<
        "1 1 1 1 1 21 " <<
        "1 1 2 1 1 23 " <<
        "1 1 4 1 1 24 ";

    int result = run(input);

    EXPECT_EQ(result, 2);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
